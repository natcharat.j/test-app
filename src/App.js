import logo from "./logo.svg";
import "./App.css";

function App() {
  const isValid = function (inputStr) {
    const map = {
      "(": ")",
      "{": "}",
      "[": "]",
    };
    const char = [];

    for (let a = 0; a < inputStr.length; a++) {
      if (map[inputStr[a]]) {
        char.push(map[inputStr[a]]);
      } else {
        if (char.pop() !== inputStr[a]) {
          return false;
        }
      }
    }

    return char.length === 0;
  };

  console.log(`isValid("([{}])")`, isValid("([{}])"));
  console.log(`isValid("[({})](]"`, isValid("[({})](]"));

  const fibs = (limit, a = 1, b = 1) => {
    return a > limit ? [] : [a, ...fibs(limit, b, a + b)];
  };

  console.log(`fibs(15)`, fibs(15));
  console.log(`fibs(8)`, fibs(8));
  console.log(`fibs(3)`, fibs(3));

  function countDuplicates(str) {
    let array = str.toLowerCase().split("");
    let unique = [...new Set(array)];
    let duplicates = unique.filter(
      (value) => array.filter((str) => str === value).length > 1
    ).length;

    return duplicates;
  }

  console.log(`countDuplicates("abcde")`, countDuplicates("abcde"));
  console.log(
    `countDuplicates(Indivisibilities)`,
    countDuplicates("Indivisibilities")
  );

  return <div className="App"></div>;
}

export default App;
